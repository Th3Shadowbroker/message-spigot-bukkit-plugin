/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Versions;

import net.minecraft.server.v1_9_R2.IChatBaseComponent;
import net.minecraft.server.v1_9_R2.PacketPlayOutChat;

import net.th3shadowbroker.AtMessage.Abstract.VersionBoundClass;
import net.th3shadowbroker.AtMessage.Util.ChatMessageBuilder;

import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Json_1_9_4 extends VersionBoundClass
{

    //Json-based Chat-Serializer for version 1.11
    @Override
    public void sendJson( Player sender, Player target, String presetSender, String presetTarget ) 
    {
        
        //Message to sender
        IChatBaseComponent SenderSerializer = IChatBaseComponent.ChatSerializer.a( ChatMessageBuilder.parseJson( presetSender , target ) );
        PacketPlayOutChat SenderPackage = new PacketPlayOutChat( SenderSerializer ); 
        ( (CraftPlayer) sender).getHandle().playerConnection.sendPacket( SenderPackage );
        
        //Message to target
        IChatBaseComponent TargetSerializer = IChatBaseComponent.ChatSerializer.a( ChatMessageBuilder.parseJson( presetTarget , sender ) );
        PacketPlayOutChat TargetPackage = new PacketPlayOutChat( TargetSerializer ); 
        ( (CraftPlayer) target).getHandle().playerConnection.sendPacket( TargetPackage );
        
    }
    
    @Override
    public void sendBroadcast(Player target, String presetTarget) 
    {
     
        //Message to target
        IChatBaseComponent TargetSerializer = IChatBaseComponent.ChatSerializer.a( ChatMessageBuilder.parseBroadcast( presetTarget , target ) );
        PacketPlayOutChat TargetPackage = new PacketPlayOutChat( TargetSerializer ); 
        ( (CraftPlayer) target).getHandle().playerConnection.sendPacket( TargetPackage );
        
    }
    
    @Override
    public void sendCommand( Player target, String command, String message ) 
    {
        
        //Message to target
        IChatBaseComponent TargetSerializer = IChatBaseComponent.ChatSerializer.a( ChatMessageBuilder.parseCommand(command, target, command) );
        PacketPlayOutChat TargetPackage = new PacketPlayOutChat( TargetSerializer ); 
        ( (CraftPlayer) target).getHandle().playerConnection.sendPacket( TargetPackage );
        
    }

}
