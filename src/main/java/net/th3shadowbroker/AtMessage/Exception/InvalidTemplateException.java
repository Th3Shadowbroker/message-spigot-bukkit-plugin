/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Exception;

@Deprecated
public class InvalidTemplateException extends Exception
{
    
    //Just a constructor
    public InvalidTemplateException(){}  
    
    //Constuctor & Message
    public InvalidTemplateException( String message )
    {
        super( message );
    }
    
}
