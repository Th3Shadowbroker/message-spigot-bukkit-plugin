/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

import java.io.File;
import java.io.IOException;

import net.th3shadowbroker.AtMessage.AtMessage;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Language 
{

    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    //The language file as file-config
    private final FileConfiguration Lang;
    
    //Current instance
    private final AtMessage Plugin = AtMessage.getInstance();
    
    //The language file as file
    private final File LangFile;
    
    //</editor-fold>
    
    //Construction
    public Language( String langCode )
    {
        
        //Language file
        LangFile = new File( AtMessage.getInstance().getDataFolder() + "/languages/" + langCode + ".yml" );
        Lang = YamlConfiguration.loadConfiguration( LangFile );
        
        //Little setup
        Lang.options().header( AtMessage.getInstance().getName() + " | Language " + langCode.toUpperCase() );
        this.Save();
                
    }
    
    //Set text once
    public void setOnce( String path, String value )
    {
        if ( Lang.get( path ) == null )
        {
            Lang.set( path, value );
            this.Save();
        }
    }
    
    //Set text
    public void set( String path, String value )
    {
        Lang.set( path, value );
        this.Save();
    }
    
    //Get text
    public String get( String path )
    {
        if ( Lang.get( path ) != null )
        {
            return Lang.getString( path ).replaceAll( "&" , "§" );
        }
        else
        {
            return "&cMessage (" + path + ") not set!".replaceAll( "&" , "§" );
        }
    }
    
    //Save config
    public void Save()
    {   
        try {
            Lang.save( LangFile );
        } catch (IOException ex) {
            Console.print( "Error while writing to " + LangFile.getName() );
            Plugin.ExceptionLog.println( ex.getMessage() );
        }  
    }
    
}
