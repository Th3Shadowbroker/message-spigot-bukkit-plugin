/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

public class Colored 
{
    
    //Parse colored string
    public static String parseString( String s )
    {
        return s.replaceAll( "&" , "§" );
    }
    
    //Parse colored string and add whitespace
    public static String parseString( String s, boolean whitespace )
    {
        if ( whitespace )
        {
            return parseString( s ) + " ";
        }
        else
        {
            return parseString( s );
        }
    }
            
}
