/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import net.th3shadowbroker.AtMessage.AtMessage;

public class LogFile 
{
    
    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    //Log-file
    private final File LFile;
    
    //Current instance
    private final AtMessage Plugin = AtMessage.getInstance();
    
    //</editor-fold>
    
    //Construction
    public LogFile( File file )
    {
        this.LFile = file;
        this.LFile.getParentFile().mkdirs();
    }
    
    //Print something to log
    public void println( String s )
    {
        
        FileWriter Writer = null;

        SimpleDateFormat TimeFormatter = new SimpleDateFormat( "hh:mm:ss" );
        SimpleDateFormat DayFormatter = new SimpleDateFormat( "MM/DD/YY" );
        
        String Time = TimeFormatter.format( new Date() );
        String Day = DayFormatter.format( new Date() );
        String TimeStamp = "[" + Time + "@" + Day + "] ";
        
        //Try writing
        try
        {
            
            Writer = new FileWriter( LFile, true );

            Writer.append( TimeStamp + s + '\n' + '\n' );            
            
        }
        
        //Catch exceptions
        catch ( Exception ex )
        {
            Console.print( "Error while writing to log-file (" + LFile.getName() + ")..." );
            Plugin.ExceptionLog.println( ex.getMessage() );
        }
        
        //Finish it up!
        finally
        {
            //Close writer
            if ( Writer != null )
            {
                try
                {
                    Writer.close();
                }
                catch ( Exception ex )
                {
                   Console.print( "Error while writing to log-file (" + LFile.getName() + ")..." );
                   Plugin.ExceptionLog.println( ex.getMessage() );
                }
            }
        }
    }
    
}
