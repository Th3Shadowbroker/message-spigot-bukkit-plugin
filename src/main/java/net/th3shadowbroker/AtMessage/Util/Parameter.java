/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

public class Parameter 
{
    
    //Replace parameters
    public static String SetParameter( String name, String raw, String repl )
    {
        
        String ParameterString = "%" + name + "%";
        
        return raw.replaceAll( ParameterString , repl );
        
    }
    
}
