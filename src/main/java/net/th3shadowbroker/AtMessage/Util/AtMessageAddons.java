/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

import java.util.ArrayList;
import java.util.List;
import net.th3shadowbroker.AtMessage.API.AtMessageAddon;
import net.th3shadowbroker.AtMessage.AtMessage;

/**
 * This class acts as @Message's Addon-Manager.
 * 
 * @author Jens
 * @since 12.01.2017
 */
public class AtMessageAddons 
{
    
    //Current plugin-instance
    private final AtMessage Plugin = AtMessage.getInstance();
    
    //A list of addons
    private List<AtMessageAddon> Addons = new ArrayList<AtMessageAddon>();
    
    //Current instance
    private static AtMessageAddons Instance;
    
    //Construction
    public AtMessageAddons() throws Exception
    {
        if ( Instance == null )
        {
            Instance = this;
        }
        else
        {
            throw new Exception( "The addon-manager is already active and persistent!" );
        }
    }
    
    /**
     * Register addon (internal).
     * 
     * @param addon The addon to register.
     */
    public void registerAddon( AtMessageAddon addon )
    {
        if ( !Addons.contains( addon ) )
        {
            Console.print( 
                            "Enabling addon " + addon.getInfo(AtMessageAddon.Information.NAME) +
                            " | Version " + addon.getInfo(AtMessageAddon.Information.VERSION) + "!"
                         );
            Addons.add( addon );
        }
    }
    
    /**
     * Unregister addon (internal).
     * 
     * @param addon  The addon to remove.
     */
    public void unregisterAddon( AtMessageAddon addon )
    {
        if ( Addons.contains( addon ) )
        {
            Addons.remove( addon );
        }
    }
    
    /**
     * Register addon (static)
     * 
     * @param addon  The addon to remove.
     */
    public static void registerCustomAddon( AtMessageAddon addon )
    {
        AtMessageAddons.getInstance().registerAddon( addon );
    }
    
    /**
     * Unregister addon (static)
     * 
     * @param addon  The addon to remove.
     */
    public static void unregisterCustomAddon( AtMessageAddon addon )
    {
        AtMessageAddons.getInstance().unregisterAddon( addon );
    }
    
    /**
     * Get current instance.
     * 
     * @return AtMessageAddons
     */
    private static AtMessageAddons getInstance()
    {
        return Instance;
    }
    
}
