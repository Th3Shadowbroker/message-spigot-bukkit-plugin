/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

import org.bukkit.permissions.Permission;

public class Permissions 
{
    
    //Types of permission
    public static enum PermissionType
    {
        AtMSG_Admin,
        AtMSG_User
    }
    
    //Get the permissions based on enum
    public static Permission GetPermission( PermissionType permission )
    {
        switch ( permission )
        {
            
            //Admin permission
            case AtMSG_Admin:
                return new Permission( "AtMessage.admin" );
                
            //User permission
            case AtMSG_User:
                return new Permission( "AtMessage.user" );
                
            //Defaul permission
            default:
                return new Permission( "AtMessage.user" );
                
        }
    }
    
}
