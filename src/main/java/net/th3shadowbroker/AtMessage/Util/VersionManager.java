/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

import net.th3shadowbroker.AtMessage.Abstract.VersionBoundClass;
import net.th3shadowbroker.AtMessage.AtMessage;

import net.th3shadowbroker.AtMessage.Versions.*;

public class VersionManager 
{
    
    //Get the current versions json-class
    public static VersionBoundClass GetCompatibleClass()
    {
        
        String ServerVersion = AtMessage.getInstance().getServer().getVersion();
        
        //Version 1.11
        if ( ServerVersion.contains( "1.11" ) )
        {
            return new Json_1_11();
        }
        
        //Version 1.10
        else if ( ServerVersion.contains( "1.10" ) )
        {
            return new Json_1_10();
        }
        
        //Version 1.9.4
        else if ( ServerVersion.contains( "1.9.4" ) )
        {
            return new Json_1_9_4();
        }
        
        return null;
        
    }
    
}
