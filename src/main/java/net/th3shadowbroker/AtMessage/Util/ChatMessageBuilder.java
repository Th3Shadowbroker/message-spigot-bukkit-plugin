/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

import net.th3shadowbroker.AtMessage.AtMessage;
import org.bukkit.entity.Player;
import org.json.JSONObject;

public class ChatMessageBuilder 
{
    
    //Create a json-parsed json
    public static String parseJson( String message, Player target )
    {
        
        //The json object
        JSONObject Obj = new JSONObject();
        JSONObject ClickEvent = new JSONObject();
        JSONObject HoverEvent = new JSONObject();
        
        //Add requirements to the object
        Obj.accumulate( "text" , message );
            
            ClickEvent.accumulate( "action" , "suggest_command" );
            ClickEvent.accumulate( "value" , "@" + target.getName() + " " );
            
            HoverEvent.accumulate( "action", "show_text" );
            HoverEvent.accumulate( "value" , AtMessage.getInstance().getLanguage().get( "Message.Hover" ) );
            
        Obj.accumulate( "clickEvent" , ClickEvent );
        Obj.accumulate( "hoverEvent" , HoverEvent );
        
        return Obj.toString();
        
    }
    
    //Create a json-parsed json
    public static String parseBroadcast( String message, Player target )
    {
        
        //The json object
        JSONObject Obj = new JSONObject();
        JSONObject HoverEvent = new JSONObject();
        
        //Add requirements to the object
        Obj.accumulate( "text" , message );
            
            HoverEvent.accumulate( "action", "show_text" );
            HoverEvent.accumulate( "value" , AtMessage.getInstance().getLanguage().get( "Message.HoverBroadcast" ) );

        Obj.accumulate( "hoverEvent" , HoverEvent );
        
        return Obj.toString();
        
    }
    
    //Create a json-parsed json
    public static String parseCommand( String message, Player target, String command )
    {
        
        //The json object
        JSONObject Obj = new JSONObject();
        JSONObject ClickEvent = new JSONObject();
        JSONObject HoverEvent = new JSONObject();
        
        //Add requirements to the object
        Obj.accumulate( "text" , message );
            
            ClickEvent.accumulate( "action" , "run_command" );
            ClickEvent.accumulate( "value" , "/" + command );
        
            HoverEvent.accumulate( "action", "show_text" );
            HoverEvent.accumulate( "value" , Parameter.SetParameter( "command", AtMessage.getInstance().getLanguage().get( "Message.HoverCommand" ), "/" + command ) );
            
        Obj.accumulate( "clickEvent" , ClickEvent );
        Obj.accumulate( "hoverEvent" , HoverEvent );
        
        return Obj.toString();
        
    }
    
}
