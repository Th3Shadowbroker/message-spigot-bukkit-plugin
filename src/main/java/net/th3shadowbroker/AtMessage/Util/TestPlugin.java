/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

import net.th3shadowbroker.AtMessage.API.AtMessageAddon;

@AtMessageAddon.AddonInfo( owner = "@Message", name = "Test-Plugin" ,author = "M4taiori", version = "1.0.0", description = "A test addon." )
public class TestPlugin extends AtMessageAddon
{

    @Override
    public void onRegister()
    {
        Console.print( getInfo(Information.NAME) + " loaded successfully!" );
    }
    
    @Override
    public String onMessageSend(String message) 
    {
        return message;
    }
    
}
