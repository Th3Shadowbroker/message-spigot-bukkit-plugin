/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

import net.th3shadowbroker.AtMessage.AtMessage;

public class Console 
{
    
    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    //Current console-prefix
    private static String ConsolePrefix = AtMessage.getInstance().ConsolePrefix();
    
    //</editor-fold>
    
    //Write to console
    public static void print( String s )
    {
        System.out.println( ConsolePrefix + s );
    }
    
    //Change console-prefix
    public static void customizePrefix( String s )
    {
        ConsolePrefix = s;
    }
    
}
