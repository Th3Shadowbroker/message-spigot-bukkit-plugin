/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Util;

import net.th3shadowbroker.AtMessage.AtMessage;
import net.th3shadowbroker.AtMessage.Abstract.VersionBoundClass;
import net.th3shadowbroker.AtMessage.MySql.MySqlSettings;

import org.bukkit.entity.Player;

public class Message 
{
    
    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    //Message sender
    private final Player Sender;
    
    //Message tasrget
    private final Player Target;
    
    //Message string
    private final String Message;
    
    //Preset: Sender
    private String PresetSender;
    
    //Preset: Target
    private String PresetTarget;
    
    //Preset: Spy
    private String PresetSpy;
    
    //Current instance
    private static final AtMessage Plugin = AtMessage.getInstance();
    
    //</editor-fold>
    
    //Construction
    public Message( Player sender, Player target, String message )
    {
        
        this.Sender = sender;
        
        this.Target = target;
        
        this.Message = message.replaceAll( "&", "§" );
        
        try
        {
            this.loadPresets();
        }
        catch ( Exception ex )
        {
            Console.print( "Error while loading presets..." );
            Plugin.ExceptionLog.println( ex.getMessage() );
        }
        
    }
    
    //Send the messsages
    public void SendMessage()
    {
        
        //Get current version json-class
        VersionBoundClass MessageParser = VersionManager.GetCompatibleClass();
        
        MessageParser.sendJson( Sender, Target, PresetSender, PresetTarget );
        
        if ( MySqlSettings.isEnabled() )
        {
            Plugin.Database.writeLogEntry( Sender, Target, Message );
        }

        //Send message to spies
        for ( Player Spy : Plugin.getSpies() )
        {
            Spy.sendMessage( PresetSpy );
        }
        
        
    }
    
    //Load presets
    private void loadPresets()
    {

        //Set presets
        this.PresetSender = buildPreset( "ToSender" );
        
        this.PresetTarget = buildPreset( "ToTarget" );
        
        this.PresetSpy = buildPreset( "ToSocialSpy" );
        
    }
    
    //Check the target
    public static boolean TargetAvailable( String name )
    {
        try
        {
            
            //Player you're looking for
            Player target = Plugin.getServer().getPlayer( name );
            
            //Target is online
            if ( target.isOnline() )
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        
        //Something went wrong
        catch ( Exception ex )
        {
            return false;
        }
    }
    
    //Get sender preset
    private String buildPreset( String presetName )
    {
        
        //The template
        String Template = Plugin.getConfig().getString( "Template." + presetName );
        
        //Insert parameters
        Template = Template.replaceAll( "%sender%" , Sender.getName() );
        
        Template = Template.replaceAll( "%target%" , Target.getName() );
        
        Template = Template.replaceAll( "%message%" , Message );
        
        //Return it
        return Colored.parseString( Template );
        
    }
    
}
