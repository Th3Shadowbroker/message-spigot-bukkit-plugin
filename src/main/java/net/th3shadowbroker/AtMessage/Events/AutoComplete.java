/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Events;

import net.th3shadowbroker.AtMessage.AtMessage;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;

import java.util.List;
import java.util.ArrayList;

public class AutoComplete implements Listener
{
    
    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    //Current instance
    private final AtMessage Plugin = AtMessage.getInstance();
    
    //</editor-fold>
    
    //Construction
    public AutoComplete()
    {
        
    }
    
    //The event
    @EventHandler
    public void onTabComplete( PlayerChatTabCompleteEvent ev )
    {
        
        //Current content
        String current = ev.getChatMessage();
        
        //Check for @Message content
        if ( 
                current.split( " " )[0].startsWith( "@" ) &&
                current.split( " " )[0].length() > 1
           )
        {
            
            String begin = current.split( " " )[0].replace( "@" , "" );
            
            //Add every player to completion list
            for ( Player player : GetPlayers( begin ) )
            {
                ev.getTabCompletions().add( "@" + player.getName() );
            }
            
        }
        
    }
    
    //Get online players
    private List<Player> GetPlayers( String startsWith )
    {
        
        //Player-list
        List<Player> Tmp = new ArrayList<Player>();
        
        //Add players to list
        for ( Player player : Plugin.getServer().getOnlinePlayers() )
        {
            if ( player.getName().startsWith( startsWith ) )
            {
                Tmp.add( player );
            }
        }
        
        return Tmp;
            
    }
    
}
