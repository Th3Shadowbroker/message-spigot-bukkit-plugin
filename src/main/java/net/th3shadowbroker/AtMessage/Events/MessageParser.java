/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Events;

import net.th3shadowbroker.AtMessage.AtMessage;

import net.th3shadowbroker.AtMessage.Util.Parameter;
import net.th3shadowbroker.AtMessage.Util.Permissions;
import net.th3shadowbroker.AtMessage.Util.Message;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class MessageParser implements Listener
{
    
    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    //Current instance
    private final AtMessage Plugin = AtMessage.getInstance();
    
    //Chat prefix
    private final String ChatPrefix = Plugin.ChatPrefix();
    
    //</editor-fold>
    
    //The event
    @EventHandler
    public void onPlayerChat( AsyncPlayerChatEvent ev )
    {
        
        Player player = ev.getPlayer();        
        String raw = ev.getMessage();
        String targetName = ev.getMessage().split( " " )[0].replace( "@" , "" );
        String ChatPrefix = Plugin.ChatPrefix();

        //Maybe this message is a @Message
        if ( 
                raw.startsWith( "@" ) && 
                raw.split( " " )[0].length() > 1
           )
        {
            
            ev.setCancelled( true );
            
            //Player has permission
            if ( player.hasPermission( Permissions.GetPermission( Permissions.PermissionType.AtMSG_User ) ) )
            {               
                try
                {
                    //Target available ?
                    if ( Message.TargetAvailable( targetName ) )
                    {
                        
                        //Message without the @<name>
                        String msg = raw.replace( "@" + targetName + " " , "" );
                        Message message = new Message( player, Plugin.getServer().getPlayer( targetName ), msg );
                        
                        //Send the message
                        message.SendMessage();
                        
                    }
                    else
                    {
                        //Send error-message
                        player.sendMessage( ChatPrefix + Parameter.SetParameter( "target", Plugin.getLanguage().get( "Error.UnknownPlayer" ), targetName ) );
                    }   
                }
                catch ( Exception ex )
                {
                    
                    //Write error to player
                    Plugin.ExceptionLog.println( ex.getMessage() );
                    player.sendMessage( ChatPrefix + Parameter.SetParameter( "target", Plugin.getLanguage().get( "Error.UnknownPlayer" ), targetName ) );
                }
            }
            else
            {
                //Player has no permission
                player.sendMessage( ChatPrefix + Plugin.getLanguage().get( "Error.NoPermission" ) );
            }
        }
        
    }
    
}
