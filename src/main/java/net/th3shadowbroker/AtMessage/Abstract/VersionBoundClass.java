/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Abstract;

import org.bukkit.entity.Player;

public abstract class VersionBoundClass 
{
    
    //Parse message as json and send it to target
    public abstract void sendJson( Player sender, Player target, String presetSender, String presetTarget );
            
    //Parse message as json and send it as broadcast
    public abstract void sendBroadcast( Player target, String presetTarget );
    
    //Parse message as json and execute command
    public abstract void sendCommand( Player target, String command, String message );
    
}
