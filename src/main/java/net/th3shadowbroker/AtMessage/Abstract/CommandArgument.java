/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Abstract;

import net.th3shadowbroker.AtMessage.AtMessage;
import org.bukkit.entity.Player;

public abstract class CommandArgument 
{

    //Current instance
    public final AtMessage Plugin = AtMessage.getInstance();
    
    //Chat prefix
    public final String ChatPrefix = Plugin.ChatPrefix();
        
    //Console prefix
    public final String ConsolePrefix = Plugin.ConsolePrefix();
    
    //Where the real action happens
    public abstract boolean onAct( Player player, String[] args );
        
}
