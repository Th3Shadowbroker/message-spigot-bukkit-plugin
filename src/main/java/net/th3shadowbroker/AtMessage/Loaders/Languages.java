/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Loaders;

import java.io.File;
import net.th3shadowbroker.AtMessage.AtMessage;
import net.th3shadowbroker.AtMessage.Util.Console;
import net.th3shadowbroker.AtMessage.Util.Language;

public class Languages 
{

    //Current instance
    AtMessage Plugin = AtMessage.getInstance();
    String CurrentLanguageCode = Plugin.getConfig().getString( "Chat.Language" );
    
    //Construction
    public Languages()
    {
        
        //Set languages
        this.setLanguages();
        
        //Load languages                
        this.loadLanguages();
                        
    }
    
    //Set languages
    private void setLanguages()
    {
        
        //English-Language
        Language EN = new Language( "en" );
        
        //Default-Messages
        EN.setOnce( "Command.Spy.AddSpy" , "&2You've been added to the spy-list!" );
        EN.setOnce( "Command.Spy.RemoveSpy" , "&cYou've been removed from spy-list!" );
        
        //Hover-Message
        EN.setOnce( "Message.Hover" , "&bClick to reply!" );
        EN.setOnce( "Message.HoverBroadcast" , "&cServer broadcast" );
        EN.setOnce( "Message.HoverCommand" , "&2Perform &e%command%" );
        
        //Error-Massages       
        EN.setOnce( "Error.NotEnoughArguments" , "&cNot enough arguments!" );
        EN.setOnce( "Error.MainCommandUsage" , "&9Please use &e%usage%" );
        EN.setOnce( "Error.InvalidArguments" , "&cInvalid arguments!" );
        EN.setOnce( "Error.NoPermission" , "&cYou're not permitted to use @Message!" );
        EN.setOnce( "Error.UnknownPlayer" , "&cPlayer &e%target% &cnot found!" );
        
    }
    
    //Load languages
    private void loadLanguages()
    {
        for ( File file : new File( Plugin.getDataFolder() + "/languages" ).listFiles() )
        {
            if ( file.getName().toLowerCase().replaceAll( ".yml" , "" ).equals( CurrentLanguageCode.toLowerCase() ) )
            {
                Console.print( "Loading locale " + CurrentLanguageCode.toLowerCase() + "..." );
                Plugin.setLanguage( new Language( file.getName().toLowerCase().replaceAll( ".yml" , "" ) ) );
                return;
            }
        }
        Console.print( "Language file not found. Disabling." );
        Plugin.getServer().getPluginManager().disablePlugin( Plugin );
    }
    
}
