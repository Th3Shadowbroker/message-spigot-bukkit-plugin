/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Loaders;

//AtMessage imports
import net.th3shadowbroker.AtMessage.AtMessage;
import net.th3shadowbroker.AtMessage.Commands.AtMessageCommand;

public class Commands 
{

    //Construction
    public Commands()
    {
        
        //Register commands
        this.registerCommands();
        
    }
    
    //Register all commands
    private void registerCommands()
    {
        
        //Current instance
        AtMessage Plugin = AtMessage.getInstance();
        
        //Default-command
        Plugin.getCommand( "amsg" ).setExecutor( new AtMessageCommand() );
        
    }
    
}
