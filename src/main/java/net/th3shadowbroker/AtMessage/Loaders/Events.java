/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Loaders;

import net.th3shadowbroker.AtMessage.AtMessage;
import net.th3shadowbroker.AtMessage.Events.AutoComplete;
import net.th3shadowbroker.AtMessage.Events.MessageParser;

public class Events 
{
    
    //Construction
    public Events()
    {
        
        registerEvents();
        
    }
    
    //Register events to plugin
    private void registerEvents()
    {
        
        AtMessage Plugin = AtMessage.getInstance();
        
        //Message parser
        Plugin.getServer().getPluginManager().registerEvents( new MessageParser() , Plugin );
        
        //Auto complete
        Plugin.getServer().getPluginManager().registerEvents( new AutoComplete() , Plugin );
        
    }
    
}
