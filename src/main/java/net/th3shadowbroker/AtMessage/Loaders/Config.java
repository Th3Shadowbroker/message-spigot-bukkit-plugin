/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Loaders;

import net.th3shadowbroker.AtMessage.AtMessage;
import org.bukkit.configuration.file.FileConfiguration;

public class Config 
{
 
    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    //Current plugin-instance
    private AtMessage AtMessage = null;
    
    //Current plugins config
    private final FileConfiguration Config = AtMessage.getInstance().getConfig();
    
    //</editor-fold>
    
    //Construction
    public Config()
    {
        
        //Set instance
        this.AtMessage = AtMessage.getInstance();
        
        //Load defaults
        this.loadDefaults();
        
        //Save config
        this.saveConfig();
        
    }
    
    //Load defaults
    private void loadDefaults()
    {
        
        //Set header
        Config.options().header( AtMessage.getName() + " | Version " + AtMessage.getDescription().getVersion() );
        
        //Chat settings
        Config.addDefault( "Chat.ConsolePrefix" , "[@Message]" );
        Config.addDefault( "Chat.ChatPrefix" , "&9[&e@&9Message]&f" );
        Config.addDefault( "Chat.Language" , "en" );
        
        //Logging settings
        Config.addDefault( "Chat.MySql.Enabled" , false );
        Config.addDefault( "Chat.MySql.Host" , "localhost" );
        Config.addDefault( "Chat.MySql.Port" , 3306 );
        Config.addDefault( "Chat.MySql.Username" , "root" );
        Config.addDefault( "Chat.MySql.Password" , "none" );
        Config.addDefault( "Chat.MySql.Database" , "test" );
        Config.addDefault( "Chat.MySql.Table" , "AtMessage" );
        
        //Templates
        Config.addDefault( "Template.ToSender" , "&9[You ═> %target%]&f %message%" );
        Config.addDefault( "Template.ToTarget" , "&9[%sender% ═> You]&f %message%" );
        Config.addDefault( "Template.ToSocialSpy" , "&e[%sender% ═> %target%]&f %message%" );
        Config.addDefault( "Template.Broadcast" , "&e[%sender%]&f %message%" );
        
        //Set options
        Config.options().copyDefaults( true );
        this.saveConfig();
        
    }
    
    //Save config
    private void saveConfig()
    {
        AtMessage.saveConfig();
    }
    
}
