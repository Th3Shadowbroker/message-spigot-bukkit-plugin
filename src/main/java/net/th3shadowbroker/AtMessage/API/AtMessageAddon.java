/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.API;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import net.th3shadowbroker.AtMessage.AtMessage;
import org.bukkit.Server;

/**
 * Abstract class for AtMessage-Addons.
 * 
 * @author Jens
 * @since 12.01.2017
 */
public abstract class AtMessageAddon
{
    
    /**
     * Current instance and variables
     */
    public AtMessage Plugin = AtMessage.getInstance();
    public Server Server = Plugin.getServer();
    
    /**
     * Contains informations
     */
    @Retention( RetentionPolicy.RUNTIME )
    public abstract @interface AddonInfo
    {
        /**
         * Owner plugin.
         * @return Plugin
         */
        public String owner();
        
        /**
         * Addon's name.
         * @return String
         */
        public String name();
        
        /**
         * Author's name.
         * @return String
         */
        public String author();
        
        /**
         * Addon's version.
         * @return String
         */
        public String version();
        
        /**
         * Addons's description.
         * @return 
         */
        public String description();
        
    }
    
    /**
     * Information types.
     */
    public enum Information
    {
        OWNER,
        NAME,
        AUTHOR,
        VERSION,
        DESCRIPTION
    }
    
    /**
     * Will be triggered by @Message on regisration.
     */
    public abstract void onRegister();
    
    /**
     * Will be triggerd by @Message.
     * 
     * @param message Will be defined by @Message.
     * @return String (Return the String you parsed.)
     */
    public abstract String onMessageSend( String message );
        
    /**
     * Get Addon's informations.
     * 
     * @param type Type of information.
     * @return String
     */
    public String getInfo( Information type )
    {
        
        AddonInfo Info;
        
        if ( this.getClass().isAnnotationPresent( AddonInfo.class ) )
        {
            Info = this.getClass().getAnnotation( AddonInfo.class );
        }
        else
        {
            return null;
        }
        
        switch( type )
        {
            
            case AUTHOR:
                return Info.author();
                
            case DESCRIPTION:
                return Info.description();
                
            case VERSION:
                return Info.version();
                
            case OWNER:
                return Info.owner();
                
            case NAME:
                return Info.name();
         
            default:
                return null;
                
        }
        
    }
    
}
