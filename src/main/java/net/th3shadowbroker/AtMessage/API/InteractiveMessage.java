/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.API;

import net.th3shadowbroker.AtMessage.Abstract.VersionBoundClass;
import net.th3shadowbroker.AtMessage.AtMessage;
import net.th3shadowbroker.AtMessage.Util.VersionManager;
import org.bukkit.entity.Player;

/**
 * Sending interactive message from another plugin.
 * 
 * @author Jens F.
 * @since 18.01.2017
 */
public class InteractiveMessage 
{
    
    private static final AtMessage Plugin = AtMessage.getInstance();
    
    /**
     * Send a json parsed message to player.
     * 
     * @param player The target player.
     * @param sender The senders name.
     * @param text The text.
     */
    public static void sendPlayerBroadcast( Player player, String sender, String text )
    {
        //Required parsing class
        VersionBoundClass MessageParser = VersionManager.GetCompatibleClass();
        
        //Template
        String Template = Plugin.getConfig().getString( "Template.Broadcast" );
        
        //Apply template
        Template = Template.replaceAll( "%sender%" , sender );
        
        Template = Template.replaceAll( "%message%" , text );
        
        Template = Template.replaceAll( "&" , "§" );
        
        MessageParser.sendBroadcast( player, Template );
    }
    
    /**
     * Send json-parsed command execution.
     * 
     * @param player The target player.
     * @param message The message you wanna link with json.
     * @param command The command to execute.
     */
    public static void sendCommandExecution( Player player, String message, String command )
    {
        
        //Required parsing class
        VersionBoundClass MessageParser = VersionManager.GetCompatibleClass();
        
        MessageParser.sendCommand( player, command, message );
        
    }
    
}
