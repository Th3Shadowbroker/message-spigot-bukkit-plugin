/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Arguments;

import net.th3shadowbroker.AtMessage.API.InteractiveMessage;
import net.th3shadowbroker.AtMessage.Abstract.CommandArgument;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.RegisteredListener;

public class AtMessageInfo extends CommandArgument
        
{

    @Override
    public boolean onAct( Player player, String[] args ) 
    {
        player.sendMessage( ChatPrefix + "§9" + Plugin.getName() +  "§e | Version §2" + Plugin.getDescription().getVersion() );
        player.sendMessage( ChatPrefix + "§eCreated by §9" + Plugin.getDescription().getAuthors() );
        player.sendMessage( ChatPrefix + "§2" + countListeners() + "§e active listeners." );

        return true;
    }
    
    //Count active listeners
    private int countListeners()
    {
        
        int c = 0;
        
        for ( RegisteredListener Listener : new HandlerList().getRegisteredListeners() )
        {
            if ( Listener.getPlugin().equals( Plugin ) )
            {
                c++;
            }
        }
        
        return c;
        
    }
    
}
