/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Arguments;

import net.th3shadowbroker.AtMessage.Abstract.CommandArgument;
import net.th3shadowbroker.AtMessage.AtMessage;

import org.bukkit.entity.Player;

public class AtMessageSpy extends CommandArgument
{

    //<editor-fold defaultstate="collapsed" desc="Variables">
    
    private final AtMessage Plugin = AtMessage.getInstance();
    
    //</editor-fold>

    @Override
    public boolean onAct( Player player, String[] args ) 
    {
        
        //Player is already a spy ?
        if ( !Plugin.getSpies().contains( player ) )
        {
            
            //Add player to the spy-list
            Plugin.getSpies().add( player );
            player.sendMessage( ChatPrefix + Plugin.getLanguage().get( "Command.Spy.AddSpy" ) );
            return true;
            
        }
        else
        {
            
            //Remove player from spy-list
            Plugin.getSpies().remove( player );
            player.sendMessage( ChatPrefix + Plugin.getLanguage().get( "Command.Spy.RemoveSpy" ) );
            return true;
            
        }

    }

}
