/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage;

//Plugin imports
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import net.th3shadowbroker.AtMessage.Loaders.Commands;
import net.th3shadowbroker.AtMessage.Loaders.Config;
import net.th3shadowbroker.AtMessage.Loaders.Events;
import net.th3shadowbroker.AtMessage.Loaders.Languages;
import net.th3shadowbroker.AtMessage.MySql.MySqlHandler;
import net.th3shadowbroker.AtMessage.MySql.MySqlSettings;
import net.th3shadowbroker.AtMessage.Util.AtMessageAddons;
import net.th3shadowbroker.AtMessage.Util.Colored;
import net.th3shadowbroker.AtMessage.Util.Console;
import net.th3shadowbroker.AtMessage.Util.Language;
import net.th3shadowbroker.AtMessage.Util.LogFile;
import net.th3shadowbroker.AtMessage.Util.TestPlugin;
import net.th3shadowbroker.AtMessage.Util.VersionManager;
import org.bukkit.entity.Player;

//Other imports
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.RegisteredListener;
import org.bukkit.plugin.java.JavaPlugin;

public class AtMessage extends JavaPlugin
{
    
    // <editor-fold defaultstate="collapsed" desc="Variables">
    
    //Current instance
    private static AtMessage instance = null;
    
    //Console prefix
    private String ConsolePrefix = "[@Message] ";
    
    //Chat prefix
    private String ChatPrefix = "§9[§e@§9Message]§f ";
    
    //The list of Spies
    private List<Player> Spies = new ArrayList<Player>();
    
    //Language
    private Language Language = null;
            
    //Log-file for exceptions
    public final LogFile ExceptionLog = new LogFile( new File( getDataFolder() + "/logs/Exception.log" ) );
    
    //Log-file for messages
    public final LogFile MessageLog = new LogFile( new File( getDataFolder() + "/logs/Messages.log" ) );
    
    //Addon manager
    public AtMessageAddons AddonManager = null;
    
    //MySql - access point
    public MySqlHandler Database = null;

    //</editor-fold>
  
    //<editor-fold defaultstate="collapsed" desc="On enable/On disable">
    
    //On enable
    @Override
    public void onEnable()
    {
        
        //Set instance
        instance = this;
        
        //Set console
        Console.print( "Enabling " + getName() + " | Version " + getDescription().getVersion() + "..." );
        
        //Load config
        Console.print( "Loading configuration..." );
        this.loadConfig();
        
        //Load prefixes
        Console.print( "Loading prefixes..." );
        this.loadPrefixes();
        Console.customizePrefix( ConsolePrefix );
        
        //Load languages
        Console.print( "Loading languages..." );
        this.loadLanguages();
        
        //Load commands
        Console.print( "Loading commands..." );
        this.loadCommands();
        
        //Load events
        Console.print( "Loading listeners..." );
        this.loadEvents();
        
        //Load api
        Console.print( "Prepairing API..." );
        this.loadAPI();
        
        //Load mysql
        this.loadMySql();
        
        //Check for compatibility (disable if not compatible)
        if ( VersionManager.GetCompatibleClass() == null )
        {
            Console.print( "@MESSAGE IS NOT COMPATIBLE WITH YOUR VERSION. DISABLING." );
            this.getServer().getPluginManager().disablePlugin( this );
        }

    }
    
    //On disable
    @Override
    public void onDisable()
    {
        
        //Kill remaining listeners
        int c = 0;
        for ( RegisteredListener Listener : new HandlerList().getRegisteredListeners() )
        {
            if ( Listener.getPlugin().equals( this ) )
            {
                new HandlerList().unregister( Listener.getListener() );
                c++;
            }
        }
        
        //Post status message
        Console.print( "Killed " + c + " listeners..." );
        
        //Save config and changes
        Console.print( "Saving config..." );
        saveConfig();
        Console.print( "Config saved..." );
        Console.print( "Disabled successfull!" );
        
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Loading methods">
    
    //Load config
    private void loadConfig()
    {
        
        //Load config (managed by config-loaderclass)
        Config configLoader = new Config();
        
    }
    
    //Load prefixes
    private void loadPrefixes()
    {
        
        //Load prefix for console-messages
        ConsolePrefix = Colored.parseString( getConfig().getString( "Chat.ConsolePrefix" ) , true );
        
        //Load prefix for chat-message
        ChatPrefix = Colored.parseString( getConfig().getString( "Chat.ChatPrefix" ) , true );
        
    }
    
    //Load commands
    private void loadCommands()
    {
        Commands commandLoader = new Commands();
    }
    
    //Load events
    private void loadEvents()
    {
        Events eventLoader = new Events();
    }
    
    //Load languages
    private void loadLanguages()
    {
        Languages languageLoader = new Languages();
    }
    
    //Load api
    private void loadAPI()
    {
        
        try
        {
            AddonManager = new AtMessageAddons();    
        }
        catch ( Exception ex )
        {
            Console.print( "Error while loading API. Disabling." );
            ex.printStackTrace();
            getServer().getPluginManager().disablePlugin(this);
        }
        
        AddonManager.registerAddon( new TestPlugin() );
        
    }
    
    //Load MySql
    private void loadMySql()
    {
        if ( MySqlSettings.isEnabled() )
        {
            Console.print( "Establishing MySQL connection..." );
            Database = new MySqlHandler();
        }
    }
    
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="Prefixes">
    
    //Get console prefix
    public String ConsolePrefix()
    {
        return ConsolePrefix;
    }
    
    //Get chat prefix
    public String ChatPrefix()
    {
        return ChatPrefix;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Instances and utils">

    //Set language file
    public void setLanguage( Language lang )
    {
        Language = lang;
    }
    
    //Get current language
    public Language getLanguage()
    {
        return Language;
    }
    
    //Get the list of spies
    public List<Player> getSpies()
    {
        return Spies;
    }
    
    //Get current instance
    public static AtMessage getInstance()
    {
        return instance;
    }
    
    //</editor-fold>
    
}
