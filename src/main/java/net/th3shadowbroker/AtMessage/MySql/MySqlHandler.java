/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.MySql;

import com.google.common.base.Charsets;
import java.sql.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import net.th3shadowbroker.AtMessage.AtMessage;
import net.th3shadowbroker.AtMessage.Util.Console;
import org.bukkit.entity.Player;

/**
 * Central class for MySql management.
 * 
 * @author Jens F.
 * @since 21.01.2017
 */
public class MySqlHandler 
{
    
    private final AtMessage Plugin = AtMessage.getInstance();
    private final MysqlDataSource Database;
    private Connection Connection;   
    
    /**
     * Construction
     */
    public MySqlHandler()
    {
       
        Database = new MysqlDataSource();
        
        Database.setServerName( Plugin.getConfig().getString( "Chat.MySql.Host" ) );
        Database.setPort( Plugin.getConfig().getInt( "Chat.MySql.Port" ) );

        Database.setUseUnicode(true);
        Database.setCharacterEncoding( "utf8" );
        
        Database.setDatabaseName( Plugin.getConfig().getString( "Chat.MySql.Database" ) );
        Database.setUser( Plugin.getConfig().getString( "Chat.MySql.Username" ) );
        
        if ( 
                !Plugin.getConfig().getString( "Chat.MySql.Password" ).equals( "" ) ||
                !Plugin.getConfig().getString( "Chat.MySql.Password" ).equalsIgnoreCase("none")
           )
        {
            //Database.setPassword( "Chat.MySql.Password" );
        }
        
        try {
            this.Connection = Database.getConnection();
        } catch (SQLException ex) {
            Console.print( "Error while connecing to database!" );
            Console.print( "Please check login informations!" );
            Console.print( "Disabling MySql..." );
            MySqlSettings.disableMySql();
            ex.printStackTrace();
        }
        
        this.setupTable();
        
    }
    
    /**
     * Create table if does no exist.
     */
    private void setupTable()
    {
        String Request = 
        "CREATE TABLE IF NOT EXISTS " + Plugin.getConfig().getString( "Chat.MySql.Table" ) +
        "(" +
        "   Date    VARCHAR(128)," +
        "   Sender  VARCHAR(256)," +
        "   Target  VARCHAR(256)," +
        "   Message VARCHAR(512)" +
        ")";
        
        try 
        {
            Statement RequestPackage = Connection.createStatement();
            RequestPackage.execute( Request );
        } catch (SQLException ex) {
            Console.print( "Error while creating table!" );
            Console.print( "Disabling MySql..." );
            MySqlSettings.disableMySql();
            ex.printStackTrace();
        }
        
    }
    
    /**
     * Write entry to table.
     * 
     * @param message The sent message.
     * @param sender The player who send the message.
     * @param target The player who received the message.
     */
    public void writeLogEntry( Player sender, Player target, String message  )
    {
        SimpleDateFormat TimestampFormatter = new SimpleDateFormat( "hh:mm:ss a @ MM.DD.YYYY" );
        String Timestamp = TimestampFormatter.format( new Date() );

        String Request = "INSERT INTO `" + Plugin.getConfig().getString( "Chat.MySql.Table" ) + "`(`Date`, `Sender`, `Target`, `Message`)"
                + " VALUES ("
                + "'" + Timestamp + "',"
                + "'" + sender.getUniqueId().toString() + "',"
                + "'" + target.getUniqueId().toString() + "',"
                + "'" + message.getBytes().toString() + "'"
                + ")";
        
        try {
            Statement RequestPackage = Connection.createStatement();
            RequestPackage.execute( Request );
        } catch (SQLException ex) {
            Console.print( "Error while writing to table!" );
            Console.print( "Disabling MySql..." );
            MySqlSettings.disableMySql();
            ex.printStackTrace();
        }
    }
    
    /**
     * Delete whole log.
     */
    public void deleteLog()
    {
        
        String Request = "DELETE FROM '" + Plugin.getConfig().getString( "Chat.MySql.Table" ) + "' WHERE 1";
        
        try {
            Statement RequestPackage = Connection.createStatement();
            RequestPackage.execute( Request );
        } catch (SQLException ex) {
            Console.print( "Error while deleting log from table!" );
            Console.print( "Disabling MySql..." );
            MySqlSettings.disableMySql();
            ex.printStackTrace();
        }
        
    }
    
    /**
     * Delete log entries for specic player.
     * 
     * @param uuid The player's uuid you want do delete from the logs.
     */
    public void deleteLogFor( UUID uuid )
    {
        
        String RequestForSender = "DELETE FROM '" + Plugin.getConfig().getString( "Chat.MySql.Table" ) + "' WHERE Sender = '" + uuid.toString() + "'";
        String RequestForTarget = "DELETE FROM '" + Plugin.getConfig().getString( "Chat.MySql.Table" ) + "' WHERE Target = '" + uuid.toString() + "'";
        
        try {
            Statement RequestPackage = Connection.createStatement();
            RequestPackage.execute( RequestForSender );
            RequestPackage.execute( RequestForTarget );
        } catch (SQLException ex) {
            Console.print( "Error while deleting log from table!" );
            Console.print( "Disabling MySql..." );
            MySqlSettings.disableMySql();
            ex.printStackTrace();
        }
        
    }

}
