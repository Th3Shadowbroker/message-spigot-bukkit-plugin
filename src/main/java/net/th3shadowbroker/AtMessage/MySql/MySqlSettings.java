/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.MySql;

import net.th3shadowbroker.AtMessage.AtMessage;

/**
 * Manages mysql-settings.
 * 
 * @author Jens F.
 * @since 21.01.2017
 */
public class MySqlSettings 
{
    
    private static final AtMessage Plugin = AtMessage.getInstance();
    
    /**
     * MySql enabled
     * 
     * @return boolean
     */
    public static boolean isEnabled()
    {
        return Plugin.getConfig().getBoolean( "Chat.MySql.Enabled" );
    }
    
    /**
     * Disable MySql
     */
    public static void disableMySql()
    {
        Plugin.getConfig().set( "Chat.MySql.Enabled", false );
    }
    
    /**
     * Enable MySql
     */
    public static void enableMySql()
    {
        Plugin.getConfig().set( "Chat.MySql.Enabled", true );
    }
    
}
