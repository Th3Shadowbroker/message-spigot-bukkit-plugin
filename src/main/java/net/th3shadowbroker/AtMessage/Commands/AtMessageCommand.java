/*
 * @Message
 * Created by Th3Shadowbroker | http://m4taiori.de
 * Licensed under CC BY-NC-SA 4.0 | https://creativecommons.org/licenses/by-nc-sa/4.0/ 
 */
package net.th3shadowbroker.AtMessage.Commands;

import net.th3shadowbroker.AtMessage.Arguments.AtMessageInfo;
import net.th3shadowbroker.AtMessage.Arguments.AtMessageSpy;
import net.th3shadowbroker.AtMessage.AtMessage;
import net.th3shadowbroker.AtMessage.Util.Parameter;
import net.th3shadowbroker.AtMessage.Util.Permissions;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AtMessageCommand implements CommandExecutor
{
    
    //Current instance
    private final AtMessage Plugin = AtMessage.getInstance();

    //Command
    @Override
    public boolean onCommand( CommandSender sender, Command cmd, String cmdLabel, String[] args ) 
    {
        if ( sender instanceof Player )
        {
            
            //Player
            Player player = (Player) sender;
            
            //Command parsing (requires admin permission)
            if ( player.hasPermission( Permissions.GetPermission( Permissions.PermissionType.AtMSG_Admin ) ) )
            {
                
                if ( cmd.getName().equalsIgnoreCase( "AMSG" ) )
                {
                    
                    //No argument given
                    if ( args.length == 0 )
                    {
                        player.sendMessage( Plugin.ChatPrefix() + Plugin.getLanguage().get( "Error.NotEnoughArguments" ) );
                        player.sendMessage( Plugin.ChatPrefix() + Parameter.SetParameter( "usage" , Plugin.getLanguage().get( "Error.MainCommandUsage" ), cmd.getUsage() ) );
                        return true;
                    }
                    
                    // Arguments given
                    else
                    {
                        
                        //The first argument
                        String FirstArgument = args[0].toUpperCase();
                        
                        //Arguments
                        AtMessageInfo InfoArgument = new AtMessageInfo();
                        AtMessageSpy SpyArgument = new AtMessageSpy();
                        
                        //Parse the first argument
                        switch ( FirstArgument )
                        {
                            
                            //Info command
                            case "INFO":
                                return InfoArgument.onAct( player, args );
                                               
                            //Spy command
                            case "SPY":
                                return SpyArgument.onAct( player, args );
                                
                            //Default result
                            default:
                                player.sendMessage( Plugin.ChatPrefix() + Plugin.getLanguage().get( "Error.InvalidArguments" ) );
                                player.sendMessage( Plugin.ChatPrefix() + Parameter.SetParameter( "usage" , Plugin.getLanguage().get( "Error.MainCommandUsage" ), cmd.getUsage() ) );
                                return true;
                                
                        }
                        
                    }
                    
                }
                    
            }
            else
            {
                
                return true;
                
            }
            
        }
        
        return false;
        
    }
    
}
